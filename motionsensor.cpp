#include "motionsensor.h"
#include <sstream>
#include "service.h"

MotionSensor::MotionSensor(long sensorID,bool isActive,float maxDis , float minDis)
{
    if(sensorID < 0) throw std::invalid_argument( "Received negative value for ID !!!" );
    if (maxDis < 0.0f || minDis < 0.0f) throw std::invalid_argument( "MOTION SENSOR: Received negative value for distance" );
       m_maxDistance = maxDis;
       m_minDistance = minDis;
       m_isActive = isActive;
       m_sensorID = sensorID;
}


MotionSensor::~MotionSensor(){

}

float MotionSensor::getMaxDis() const{
    return m_maxDistance;
}

float MotionSensor::getMinDis() const{
    return m_minDistance;
}

std::string MotionSensor::activate() const{
    std::stringstream message;
    message << "MotionSensor with ID: " << m_sensorID << " has been activated.\n"
            << "Calling following services: ";
    message << Sensor::activate();

    return message.str();
}


std::string MotionSensor::getSensorType() const{
    return "Motion Sensor.";
}

const std::string MotionSensor::getInformation(){
    std::stringstream ss;
    ss << "This is a MotionSensor with ID: " << m_sensorID << ". It is currently "<< ((Sensor::getsSensorStatus())?"Activated." : "De-Activated.") << "\n";
    return ss.str();
}
