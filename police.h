#ifndef POLICE_H
#define POLICE_H
#include <string>
#include "service.h"

class Police: public Service
{
public:
    Police();
    std::string triggerService();

};

#endif // POLICE_H
