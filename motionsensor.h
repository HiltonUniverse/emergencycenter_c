#ifndef MOTIONSENSOR_H
#define MOTIONSENSOR_H
#include <string>
#include "sensor.h"

class MotionSensor: public Sensor
{
public:
    MotionSensor(long sensorID,bool isActive,float maxDis , float minDis);
    virtual ~MotionSensor();
    const std::string getInformation();
    std::string getSensorType()const;
    std::string activate() const override;
    float getMaxDis() const;
    float getMinDis() const;

private:
    float m_minDistance;
    float m_maxDistance;
};

#endif // MOTIONSENSOR_H
