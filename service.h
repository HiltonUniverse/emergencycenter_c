#ifndef SERVICE_H
#define SERVICE_H
#include <string>

class Service
{
public:
    virtual std::string triggerService() = 0;

protected:
    Service();
};

#endif // SERVICE_H
