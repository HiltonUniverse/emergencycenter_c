#include <sstream>
#include "composite.h"

//compositeName = is it building/level or room?
Composite::Composite(const std::string &compositeName):
    m_compName(compositeName)
{

}

//return the name of the composite
std::string Composite::getCompositeName() const{
    return m_compName;
}

//add a new component i.e. building,level,room or sensor
void Composite::addCOmponent(std::shared_ptr<Component>& component){
    m_componentList.push_back(component);
}

std::string Composite::activate() const{
    std::stringstream ss;
    ss << "This is a " << m_compName << "\n";
    for(const auto & temp: m_componentList){
        ss << temp->activate();
    }
    return ss.str();
}

//not sure what to do with this for now!
//now it just gives a basic informatino of the current composite
const std::string Composite::getInformation(){
    std::stringstream ss;
    ss << "This is: " << m_compName <<"\n";
    information = ss.str();
    return information;
}
