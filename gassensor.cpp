#include "gassensor.h"
#include <sstream>
#include "service.h"

GasSensor::GasSensor(long sensorID,bool isActive)
{
    m_sensorID = sensorID;
    m_isActive = isActive;
}

std::string GasSensor::activate() const{
    std::stringstream message;
    message << "GasSensor with ID: " << m_sensorID << " has been activated.\n"
            << "Calling following services: ";
    message << Sensor::activate();


    return message.str();
}

std::string GasSensor::getSensorType() const{
    return "Gas Sensor.";
}

const std::string GasSensor::getInformation(){
    std::stringstream ss;
    ss << "This is a GasSensor with ID: " << m_sensorID << ". It is currently "<< ((Sensor::getsSensorStatus())?"Activated." : "De-Activated.") << "\n";
    return ss.str();
}
