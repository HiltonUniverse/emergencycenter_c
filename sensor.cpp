#include "sensor.h"
#include "service.h"
#include <sstream>
Sensor::Sensor()
{

}

std::string Sensor::activate() const{
    std::stringstream message;
    for(const auto& serv : serviceList){
        message << serv->triggerService();
    }
    return message.str();
}

long Sensor::getSensorID()
{
    return m_sensorID;
}

bool Sensor::getsSensorStatus()
{
    return m_isActive;
}



