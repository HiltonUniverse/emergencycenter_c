#include <iostream>
#include <assert.h>
#include "emergencycenter.h"
#include "smokesensor.h"
#include "motionsensor.h"
#include "gassensor.h"
#include "police.h"
#include "firebrigade.h"
#include "evacuation.h"
#include "component.h"
#include "composite.h"

int main()
{
    EmergencyCenter* emergencyCenterInstance = EmergencyCenter::getEmergencyCenterInstance();

    std::shared_ptr<Component> groepT = std::make_shared<Composite>("GroepT");
    std::shared_ptr<Component> module = std::make_shared<Composite>("module2");
    std::shared_ptr<Component> room = std::make_shared<Composite>("room11.56");

    //std::cout << std::dynamic_pointer_cast<Composite> (module)->getInformation() << std::endl;

    std::shared_ptr<Component> sensorTest = std::make_shared<SmokeSensor>(0,true);
    std::shared_ptr<Component> sensorTest2 = std::make_shared<MotionSensor>(1,false,50,5);
    std::shared_ptr<Component> sensorTest3 = std::make_shared<GasSensor>(2,true);

    std::cout << std::dynamic_pointer_cast<Sensor> (sensorTest2)->getSensorType() << std::endl;


/*
    std::shared_ptr<Service> policeService = std::make_shared<Police>();
    std::shared_ptr<Service> fireService = std::make_shared<FireBrigade>();
    std::shared_ptr<Service> evacuaService = std::make_shared<Evacuation>();

    emergencyCenterInstance->addServiceForASensor(fireService,sensorTest);
    emergencyCenterInstance->addServiceForASensor(evacuaService,sensorTest2);

    emergencyCenterInstance->addServiceForASensor(policeService,sensorTest3);
    emergencyCenterInstance->addServiceForASensor(evacuaService,sensorTest3);


    try{
    emergencyCenterInstance->addSensorInBuilding(sensorTest,groepT);
    emergencyCenterInstance->addLevelInBuilding(module,groepT);
    emergencyCenterInstance->addSensorInLevel(sensorTest2,module);
    emergencyCenterInstance->addSensorInRoom(sensorTest3,room);
    emergencyCenterInstance->addRoomInLevel(room,module);

    emergencyCenterInstance->activateSensor(groepT);

    }catch(const char* excep){
        std::cout << excep << std::endl;
    }
    */

    //emergencyCenterInstance->activateSensor(sensorTest);

    //assert(std::dynamic_pointer_cast<Sensor> (sensorTest)->getServiceList().size() == 3);

    return 0;
}
