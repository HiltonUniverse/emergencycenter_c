#ifndef GASSENSOR_H
#define GASSENSOR_H
#include <string>
#include "sensor.h"

class GasSensor: public Sensor
{
public:
    GasSensor(long sensorID, bool isActive);
    const std::string getInformation();
    std::string getSensorType()const;
    std::string activate() const;
};

#endif // GASSENSOR_H
