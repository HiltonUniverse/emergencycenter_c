#ifndef SENSOR_H
#define SENSOR_H
#include <vector>
#include <string>
#include <memory>
#include "component.h"

class Service;

class Sensor: public Component
{
public:
    //copy constructor
    Sensor(const Sensor&) = delete;
    //assignment operator
    Sensor& operator=(const Sensor&) = delete;


    virtual std::string activate() const;

    void activateSensor();
    void deactivateSensor();

    long getSensorID();
    virtual std::string getSensorType() const = 0;
    bool getsSensorStatus();
    virtual const std::string getInformation() = 0;


    //std::ostream& operator<<(std::ostream&,const Sensor&);
    void addService(std::shared_ptr<Service> &service){serviceList.push_back(service);}

protected:
    Sensor();
    long m_sensorID;
    bool m_isActive;
    std::vector <std::shared_ptr<Service>> serviceList;
};

#endif // SENSOR_H
