#include <iostream>
#include <exception>
#include "emergencycenter.h"
#include "service.h"
#include "sensor.h"
#include "component.h"
#include "composite.h"


EmergencyCenter* EmergencyCenter::emergencyCenterInstance = nullptr;

EmergencyCenter::EmergencyCenter()
{

}

EmergencyCenter::~EmergencyCenter(){
   if(emergencyCenterInstance){
       delete emergencyCenterInstance;
       emergencyCenterInstance = nullptr;
   }
}

void EmergencyCenter::activateSensor(std::shared_ptr<Component>& component){
    std::cout << component->activate();
}

void EmergencyCenter::deactivateSensor(std::shared_ptr<Component>& sensor){

}

void EmergencyCenter::testSenosrs(std::shared_ptr<Component> sensors){

}

EmergencyCenter *EmergencyCenter::getEmergencyCenterInstance(){
    if(emergencyCenterInstance) return emergencyCenterInstance;
    emergencyCenterInstance = new EmergencyCenter;
   return emergencyCenterInstance;
}

void EmergencyCenter::addServiceForASensor(std::shared_ptr<Service> &service, std::shared_ptr<Component> &sensor){

    //Dynamic_pointer_cast = used to easily cast between pointers
      std::dynamic_pointer_cast<Sensor> (sensor)->addService(service);
}

//rhs to = component always
void EmergencyCenter::addCompositeOrLeafInComposite(std::shared_ptr<Component> &from,std::shared_ptr<Component> &to){
    if(std::dynamic_pointer_cast<Sensor> (to) == nullptr){
        std::dynamic_pointer_cast<Composite> (to)->addCOmponent(from);
    }else{
        throw "RHS cannot be sensor!";
    }
}

void EmergencyCenter::addCompositeInComposite(std::shared_ptr<Component> &from,std::shared_ptr<Component> &to){
    if(std::dynamic_pointer_cast<Sensor> (from) == nullptr){
        try{
            addCompositeOrLeafInComposite(from,to);
        }catch(const char* temp){
            throw temp;
        }
    }else{
        throw "LHS cannot be a sensor!";
    }
}


void EmergencyCenter::addLeafInComposite(std::shared_ptr<Component> &from,std::shared_ptr<Component> &to){
    if(std::dynamic_pointer_cast<Sensor> (from) != nullptr){
        try{
            addCompositeOrLeafInComposite(from,to);
        }catch(const char* temp){
            throw temp;
        }
    }else{
        throw "LHS should be a sensor!";
    }
}

//tested
void EmergencyCenter::addSensorInBuilding(std::shared_ptr<Component> &from,std::shared_ptr<Component> &to){
    try{
        addLeafInComposite(from,to);
    }catch (const char* temp){
        throw temp;
    }
}

//tested
void EmergencyCenter::addLevelInBuilding(std::shared_ptr<Component> &from,std::shared_ptr<Component> &to){
   try{
        addCompositeInComposite(from,to);
   }catch (const char* temp){
        throw temp;
   }
}

//tested
void EmergencyCenter::addSensorInLevel(std::shared_ptr<Component> &from,std::shared_ptr<Component> &to){
    try{
         addLeafInComposite(from,to);
    }catch (const char* temp){
         throw temp;
    }
}

//tested
void EmergencyCenter::addRoomInLevel(std::shared_ptr<Component> &from,std::shared_ptr<Component> &to){
    try{
         addCompositeInComposite(from,to);
    }catch (const char* temp){
         throw temp;
    }
}

void EmergencyCenter::addSensorInRoom(std::shared_ptr<Component> &from,std::shared_ptr<Component> &to){
    try{
         addLeafInComposite(from,to);
    }catch (const char* temp){
         throw temp;
    }
}


