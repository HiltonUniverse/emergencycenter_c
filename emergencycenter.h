#ifndef EMERGENCYCENTER_H
#define EMERGENCYCENTER_H
#include <vector>
#include <stdlib.h>
#include <memory>
class Component;
class Service;
class Sensor;

class EmergencyCenter
{
public:
    void activateSensor(std::shared_ptr<Component> &component);
    void deactivateSensor(std::shared_ptr<Component>&);
    void testSenosrs(std::shared_ptr<Component>);
    //testing
    void addServiceForASensor(std::shared_ptr<Service>&, std::shared_ptr<Component> &);
    static EmergencyCenter* getEmergencyCenterInstance();
    virtual ~EmergencyCenter();

    void addSensorInBuilding(std::shared_ptr<Component> &from,std::shared_ptr<Component> &to);
    void addLevelInBuilding(std::shared_ptr<Component> &from,std::shared_ptr<Component> &to);

    void addSensorInLevel(std::shared_ptr<Component> &from,std::shared_ptr<Component> &to);
    void addRoomInLevel(std::shared_ptr<Component> &from,std::shared_ptr<Component> &to);

    void addSensorInRoom(std::shared_ptr<Component> &from,std::shared_ptr<Component> &to);



private :
   EmergencyCenter();
   static EmergencyCenter* emergencyCenterInstance;
   std::vector<std::shared_ptr<Component>> componentList;
   void addCompositeOrLeafInComposite(std::shared_ptr<Component> &from,std::shared_ptr<Component> &to);
   void addCompositeInComposite(std::shared_ptr<Component> &from,std::shared_ptr<Component> &to);
   void addLeafInComposite(std::shared_ptr<Component> &from,std::shared_ptr<Component> &to);
};

#endif // EMERGENCYCENTER_H
