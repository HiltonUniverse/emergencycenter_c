#ifndef COMPOSITE_H
#define COMPOSITE_H
#include <string>
#include <vector>
#include <memory>
#include "component.h"

class Composite:public Component
{
public:
    //done
    Composite(const std::string &compositeName);
    //done
    std::string getCompositeName() const;
    //done
    void addCOmponent(std::shared_ptr<Component>&);

    const std::string getInformation();
    //done
    virtual std::string activate() const;
    //dont
    long getCompositeID() const;

  private:
    std::vector<std::shared_ptr<Component>> m_componentList;
    std::string m_compName;
};

#endif // COMPOSITE_H
