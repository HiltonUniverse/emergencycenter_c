#ifndef FIREBRIGADE_H
#define FIREBRIGADE_H
#include <string>
#include "service.h"

class FireBrigade: public Service
{
public:
    FireBrigade();
    std::string triggerService();
};

#endif // FIREBRIGADE_H
