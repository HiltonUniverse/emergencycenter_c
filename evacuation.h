#ifndef EVACUATION_H
#define EVACUATION_H
#include <string>
#include "service.h"

class Evacuation: public Service
{
public:
    Evacuation();
    std::string triggerService();
};

#endif // EVACUATION_H
