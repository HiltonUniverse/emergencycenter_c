#ifndef COMPONENT_H
#define COMPONENT_H
#include <string>

class Component
{
public:
    virtual ~Component();
    virtual std::string activate() const = 0;
    virtual const std::string getInformation() = 0;
protected:
    Component();
    std::string information;
};

#endif // COMPONENT_H
