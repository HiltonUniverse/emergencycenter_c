#ifndef SMOKESENSOR_H
#define SMOKESENSOR_H
#include <string>
#include "sensor.h"

class SmokeSensor: public Sensor
{
public:
    SmokeSensor(long sensorID,bool isActive);
    std::string activate() const override;
    std::string getSensorType() const;
    const std::string getInformation();
};

#endif // SMOKESENSOR_H
