#include "smokesensor.h"
#include "service.h"
#include <sstream>

SmokeSensor::SmokeSensor(long sensorID,bool isActive)
{
    m_sensorID = sensorID;
    m_isActive = isActive;
}

std::string SmokeSensor::activate() const{
    std::stringstream message;
    message << "SmokeSensor with ID: \n" << m_sensorID << " has been activated.\n"
            << "Calling following services: ";
    message << Sensor::activate();

    return message.str();
}

std::string SmokeSensor::getSensorType() const{
    return "Smoke Sensor.";
}

const std::string SmokeSensor::getInformation(){
    std::stringstream ss;
    ss << "This is a SmokeSensor with ID: " << m_sensorID << ". It is currently "<< ((Sensor::getsSensorStatus())?"Activated." : "De-Activated.") << "\n";
    return ss.str();
}
